my_int = 200_300_400
print(str(my_int).count('0'))

###############################

my_int = 200_300_400
my_str = str(my_int)
my_str_rev = int(my_str[::-1])
result = len(my_str) - len(str(my_str_rev))
print(result)

################################

my_int = 200_300_400
count = 0

while my_int % 10 == 0:
    count += 1
    my_int //= 10
print(count)

################################

my_list_1 = [1, 2, 3, 4, 5, 6]
my_list_2 = [11, 22, 33, 44, 55, 66]
result = my_list_1[::2] + my_list_2[1::2]
print(result)

################################

my_list = [1, 2, 3, 4]
new_list = my_list[1:] + [my_list[0]]
print(new_list)

################################

my_list = [1, 2, 3, 4]
list_value = my_list.pop(0)
my_list.append(list_value)
print(my_list)

################################

my_string = "43 7 слово 100 34 слоао 60 56"
my_str_list = my_string.split()
counter = 0

for item in my_str_list:
    if item.isdigit():
        counter += int(item)
print(counter)

################################

my_str = "My long string"
l_limit, r_limit = "o", "g"
l_index = my_str.find(l_limit) + 1
r_index = my_str.rfind(r_limit)
sub_str = my_str[l_index: r_index]
print(sub_str)

################################

my_str = 'abcde'
result_list = []
my_str_length = len(my_str)
for index in range(0, my_str_length, 2):
    if index < my_str_length - 1:
        result_list.append(my_str[index] + my_str[index + 1])
    else:
        result_list.append(my_str[index] + '_')
print(result_list)

################################

my_list = [2, 4, 1, 5, 3, 9, 0, 7]
counter = 0

for index in range(1, len(my_list) - 1):
    if my_list[index] > sum([my_list[index - 1], my_list[index + 1]]):
        counter += 1
print(counter)